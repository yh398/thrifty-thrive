import { MongoClient } from "mongodb";
import { Item, Order } from "./models";

const url = process.env.MONGODB_URL || "mongodb://localhost:27017";
const client = new MongoClient(url);

const items: Item[] = [
  {
    _id: "1",
    name: "T-shirt",
    description: "Best T-shirt ever",
    discountedPrice: 15,
    price: 20,
  },
  {
    _id: "2",
    name: "Bike",
    description: "Best bike ever",
    discountedPrice: 80,
    price: 100,
  },
  {
    _id: "3",
    name: "PC",
    description: "Best PC ever",
    discountedPrice: 155,
    price: 1333,
  },
];

const orders: Order[] = [
  {
    _id: "1",
    name: "John Doe",
    email: "jd@h.com",
    address: "123 Main St",
    creditCard: "1234-5678-9012-3456",
    items: [
      {
        _id: "1",
        name: "T-shirt",
        description: "Best T-shirt ever",
        discountedPrice: 15,
        price: 20,
      },
    ],
  },
  {
    _id: "2",
    name: "Jane",
    email: "jane@d.com",
    address: "123 Main St",
    creditCard: "1234-5678-9012-3456",
    items: [
      {
        _id: "2",
        name: "Bike",
        description: "Best bike ever",
        discountedPrice: 80,
        price: 100,
      },
    ],
  },
];

async function main() {
  await client.connect();
  console.log("Connected successfully to MongoDB");

  const db = client.db("tt");

  await db.collection("items").drop();
  await db.collection("orders").drop();

  // add data
  console.log(
    "inserting items",
    await db.collection("items").insertMany(items as any)
  );
  console.log(
    "inserting orders",
    await db.collection("orders").insertMany(orders as any)
  );

  process.exit(0);
}

main();
