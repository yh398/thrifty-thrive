import bodyParser from "body-parser";
import express, { Express, Request, Response } from "express";
import { Collection, Db, MongoClient } from "mongodb";
import PinoHttp from "pino-http";
import { Item, Order } from "./models";

// setup mongo
const url = process.env.MONGO_URL || "mongodb://127.0.0.1:27017";
const client = new MongoClient(url);
let db: Db;
let itemsCollection: Collection<Item>;
let ordersCollection: Collection<Order>;

// setup express
const app: Express = express();
const port = parseInt(process.env.PORT ?? "8191");
app.use(bodyParser.json());

// set up Pino logging
const pino = PinoHttp({
  transport: {
    target: "pino-http-print",
  },
});
app.use(pino);

app.get("/api/items", async (req: Request, res: Response) => {
  const items = await itemsCollection.find().toArray();
  res.json(items);
});

app.get("/api/orders", async (req: Request, res: Response) => {
  const orders = await ordersCollection.find().toArray();
  res.json(orders);
});

client.connect().then(() => {
  console.log("Connected successfully to MongoDB");
  db = client.db("tt");
  itemsCollection = db.collection("items");
  ordersCollection = db.collection("orders");

  // start server
  app.listen(port, () => {
    console.log(`TT server listening on port ${port}`);
  });
});
