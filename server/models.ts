export interface StringIdDocument {
  _id: string;
}

export interface Item extends StringIdDocument {
  name: string;
  description: string;
  discountedPrice: number;
  price: number;
}

export interface Order extends StringIdDocument {
  items: Item[];
  name: string;
  email: string;
  address: string;
  creditCard: string;
}
