import type { Item, Order } from '../../../server/models'

export async function getItems(): Promise<Item[]> {
  const response = await (await fetch('/api/items')).json()
  console.log(response)
  return response
}

export async function getOrders(): Promise<Order[]> {
  const response = await (await fetch('/api/orders')).json()
  return response
}
